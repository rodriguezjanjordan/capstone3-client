import {useState, useEffect, useContext, Fragment} from 'react'
import Swal from 'sweetalert2'

import Head from 'next/head'
import Router from 'next/router'

import {Card, Form, Button} from 'react-bootstrap'
import {Row, Col} from 'react-bootstrap'

import UserContext from '../../UserContext'



export default function addCategory(){
	// USER CONTEXT
	const {user, setUser} = useContext(UserContext)

	// STATES
	const [categoryType, setCategoryType] = useState('')
	const [categoryName, setCategoryName] = useState('')

	const [isActive, setIsActive] = useState(false)

	// USEEFFECT()
	useEffect(() => {
		if (categoryName !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [categoryType, categoryName])

	// ADDNEWCATEGORY()
	function addNewCategory(e){
		e.preventDefault()

		let categType = document.getElementById("categType").value

		if (categType === "Income"){
			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/categories`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					category_name: categoryName,
					category_type: 'Income'
				})
			})
			.then(res => res.json())
			.then(data => {
				if (data === true){
					Swal.fire({
						icon: 'success',
						title: `"${categoryName}" added successfully!`,
						text: 'Do you want to add more?',
						showDenyButton: true,
						confirmButtonText: `Yes`,
						denyButtonText: `Back to categories`,
					})
					.then((result) => {
						if (result.isConfirmed) {
							setCategoryType('')
							setCategoryName('')

							Router.push('/categories/addCategory')
						} else if (result.isDenied) {
							Router.push("/categories")
						}
					})
				} else {
					Swal.fire({
						icon: 'error',
						text: 'Please try again.'
					})
				}
			})
		} else if (categType === "Expense"){
			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/categories`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					category_name: categoryName,
					category_type: 'Expense'
				})
			})
			.then(res => res.json())
			.then(data => {
				if (data === true){
					Swal.fire({
						icon: 'success',
						title: `"${categoryName}" added successfully!`,
						text: 'Do you want to add more?',
						showDenyButton: true,
						confirmButtonText: `Yes`,
						denyButtonText: `Back to categories`,
					})
					.then((result) => {
						if (result.isConfirmed) {
							setCategoryType('')
							setCategoryName('')

							Router.push('/categories/addCategory')
						} else if (result.isDenied) {
							Router.push("/categories")
						}
					})
				} else {
					Swal.fire({
						icon: 'error',
						text: 'Please try again.'
					})
				}
			})
		}
	}
	
	return (
		<Fragment>
		<Head>
			<title> Add Category | Grapher+ </title>
		</Head>
		<Row>
			<Col lg={{span: 6, offset: 3}}>
				<Card id="cardform">
					<Card.Title>
						<h1> Add category </h1>
					</Card.Title>

					<Card.Body>
						<Form onSubmit={(e) => addNewCategory(e)}>
							<Form.Group>
								<Form.Label> Category Type: </Form.Label>
								<Form.Control
									as="select"
									id="categType"
									value={categoryType}
									onChange={(e) => setCategoryType(e.target.value)}
									required
								>
									<option value="" disabled> Select category type </option>
									<option value="Income"> Income </option>
									<option value="Expense"> Expense </option>
								</Form.Control>
							</Form.Group>

							<Form.Group controlId="categoryName">
								<Form.Label> Category Name: </Form.Label>
								<Form.Control
									type="text"
									placeholder="Enter category name"
									maxLength="30"
									value={categoryName}
									onChange={(e) => setCategoryName(e.target.value)}
									required
								/>
								<Form.Text className="text-muted"> Please limit your category name to 30 characters. </Form.Text>
							</Form.Group>

							{
								isActive
								?
								<Button variant="outline-dark" type="submit" block> Add new category </Button>
								:
								<Button variant="dark" type="submit" block disabled> Add new category </Button>
							}

							<Button href="/categories" variant="dark" block> Back to Categories </Button>
						</Form>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		</Fragment>
	)
}