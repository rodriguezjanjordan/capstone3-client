import {useState, useEffect, useContext, Fragment} from 'react'
import moment from 'moment'

import Head from 'next/head'
import Router from 'next/router'

import {Table, Button} from 'react-bootstrap'
import {Row, Col} from 'react-bootstrap'

import UserContext from '../../UserContext'



export default function index(){
	// USER CONTEXT
	const {user, setUser} = useContext(UserContext)

	// STATE
	const [categoryList, setCategoryList] = useState([])

	// USEEFFECT()
	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/categories`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data.length > 0){
				setCategoryList(data)
			} else {
				setCategoryList([])
			}
		})
	}, [categoryList])

	return (
		<Fragment>
		<Head>
			<title> Categories | Grapher+ </title>
		</Head>
		<h1> List of Categories </h1>
		<Row>
			<Col lg={{span: 10, offset: 1}}>
				<Button href="/categories/addCategory" className="addButton" variant="outline-dark" block>
					Add new category
				</Button>
			</Col>
		</Row>
		<Row>
			<Col lg={{span: 10, offset: 1}}>
				<Table striped bordered hover responsive variant="dark">
					<thead>
						<tr>
							<th> Category </th>
							<th> Type </th>
						</tr>
					</thead>

					<tbody>
						{
							categoryList.map(categories => {
								return (
									<tr key={categories._id}>
										<td>{categories.category_name}</td>
										<td>{categories.category_type}</td>
									</tr>		
								)
							})
						}
					</tbody>
				</Table>
			</Col>
		</Row>
		</Fragment>
	)
}