import {useState, useEffect, useContext, Fragment} from 'react'
import moment from 'moment'

import Head from 'next/head'
import Router from 'next/router'

import {Card, Form, Button} from 'react-bootstrap'
import {Row, Col} from 'react-bootstrap'

import UserContext from '../../UserContext'



export default function index(){
	// USER CONTEXT
	const {user, setUser} = useContext(UserContext)

	// STATES
	const [recordList, setRecordList] = useState('')
	const [search, setSearch] = useState('')
	const [filter, setFilter] = useState('')

	// USEEFFECT()
	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/records`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (filter === '' || filter === 'All'){
				if (search === ''){
					setRecordList(data.slice(0).reverse().map(record => {
						let recordId = record._id
						return (
							<Card key={record._id}>
								<Card.Body>
									<Row>
										<Col>
											<span>
												<h2> {record.description} </h2>
												<span>
												{
													record.category_type === "Income"
													?
													<b className="text-success"> {record.category_type} </b>
													:
													<b className="text-danger"> {record.category_type} </b>
												}
												</span>
												<span><b> ( {record.category_name} ) </b></span>
												<p> {moment(record.date).format('MMMM Do YYYY')} </p>
											</span>
										</Col>

										<Col>
										{
											record.category_type === 'Income'
											?
											<Col className="text-right">
												<p className="text-success"> Amount: $ +{record.amount} </p>
												<p><b> Balance: $ {record.record_balance} </b></p>
											</Col>
											:
											<Col className="text-right">
												<p className="text-danger"> Amount: $ -{record.amount} </p>
												<p><b> Balance: $ {record.record_balance} </b></p>
											</Col>
										}
										</Col>
									</Row>
								</Card.Body>
							</Card>
						)
					}))
				} else {
					setRecordList(data.slice(0).reverse().map(record => {
						let recordDescription = record.description
						if (recordDescription.match(search.trim())){
							return (
								<Card key={record._id}>
									<Card.Body>
										<Row>
											<Col>
												<span>
													<h2> {record.description} </h2>
													<span>
													{
														record.category_type === "Income"
														?
														<b className="text-success"> {record.category_type} </b>
														:
														<b className="text-danger"> {record.category_type} </b>
													}
													</span>
													<span><b> ( {record.category_name} ) </b></span>
													<p> {moment(record.date).format('MMMM Do YYYY')} </p>
												</span>
											</Col>

											<Col>
											{
												record.category_type === 'Income'
												?
												<Col className="text-right">
													<p className="text-success"> Amount: $ +{record.amount} </p>
													<p><b> Balance: $ {record.record_balance} </b></p>
												</Col>
												:
												<Col className="text-right">
													<p className="text-danger"> Amount: $ -{record.amount} </p>
													<p><b> Balance: $ {record.record_balance} </b></p>
												</Col>
											}
											</Col>
										</Row>
									</Card.Body>
								</Card>
							)
						}
					}))
				}
			} else if (filter === 'Income'){
				setRecordList(data.slice(0).reverse().map(record => {
					if (record.category_type === 'Income'){
						let recordDescription = record.description
						if (recordDescription.match(search.trim())){
							return (
								<Card key={record._id}>
									<Card.Body>
										<Row>
											<Col>
												<span>
													<h2> {record.description} </h2>
													<p><b className="text-success"> {record.category_type} </b> <b> ( {record.category_name} ) </b></p>
													<p> {moment(record.date).format('MMMM Do YYYY')} </p>
												</span>
											</Col>

											<Col className="text-right">
												<p className="text-success"> Amount: $ +{record.amount} </p>
												<p><b> Balance: $ {record.record_balance} </b></p>
											</Col>
										</Row>
									</Card.Body>
								</Card>
							)
						}
					}
				}))
			} else if (filter === 'Expense'){
				setRecordList(data.slice(0).reverse().map(record => {
					if (record.category_type === 'Expense'){
						let recordDescription = record.description
						if (recordDescription.match(search.trim())){
							return (
								<Card key={record._id}>
									<Card.Body>
										<Row>
											<Col>
												<span>
													<h2> {record.description} </h2>
													<p><b className="text-danger"> {record.category_type} </b> <b> ( {record.category_name} ) </b></p>
													<p> {moment(record.date).format('MMMM Do YYYY')} </p>
												</span>
											</Col>

											<Col className="text-right">
												<p className="text-danger"> Amount: $ +{record.amount} </p>
												<p><b> Balance: $ {record.record_balance} </b></p>
											</Col>
										</Row>
									</Card.Body>
								</Card>
							)
						}
					}
				}))
			}
		})
	}, [filter, search])

	return (
		<Fragment>
		<Head>
			<title> Records | Grapher+ </title>
		</Head>
		<h1> List of Records </h1>
		<Row>
			<Col md={12} lg={3}>
				<Button id="addRecBtn" href="/records/addRecord" onClick="" className="addButton RecRow" variant="outline-dark" block>
					Add new record
				</Button>
			</Col>

			<Col md={12} lg={5}>
				<Form className="RecRow">
					<Form.Group controlId="search">
						<Form.Control
							type="text"
							placeholder="Search records"
							value={search}
							onChange={(e) => setSearch(e.target.value)}
						/>
					</Form.Group>
				</Form>
			</Col>

			<Col md={12} lg={4}>
				<Form className="RecRow">
					<Form.Group>
						<Form.Control
							as="select"
							id="filter"
							value={filter}
							onChange={(e) => setFilter(e.target.value)}
						>
							<option value="" disabled> Filter records </option>
							<option value="All"> All </option>
							<option value="Income"> Income </option>
							<option value="Expense"> Expense </option>
						</Form.Control>
					</Form.Group>
				</Form>
			</Col>
		</Row>
		<Row>
			<Col>
				<h2>{recordList}</h2>
			</Col>
		</Row>
		</Fragment>
	)
}