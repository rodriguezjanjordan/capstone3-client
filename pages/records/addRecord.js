import {useState, useEffect, useContext, Fragment} from 'react'
import Swal from 'sweetalert2'

import Head from 'next/head'
import Router from 'next/router'

import {Card, Form, Button} from 'react-bootstrap'
import {Row, Col} from 'react-bootstrap'

import UserContext from '../../UserContext'



export default function addRecord(){
	// USER CONTEXT
	const {user, setUser} = useContext(UserContext)
	
	// STATES
	const [categoryList, setCategoryList] = useState('')
	const [incomeList, setIncomeList] = useState([])
	const [expenseList, setExpenseList] = useState([])

	const [categoryType, setCategoryType] = useState('')
	const [categoryName, setCategoryName] = useState('')
	const [amount, setAmount] = useState('')
	const [description, setDescription] = useState('')
	const [balance, setBalance] = useState(0)

	const [isActive, setIsActive] = useState(false)

	// USEEFFECT() - setIsActive
	useEffect(() => {
		if ((categoryType !== '' && categoryName !== '') &&
			(amount !== '' && description !== '')){
			setIsActive(true)
			checkBalance()
		} else {
			setIsActive(false)
		}
	}, [categoryName, categoryType, amount, description])

	// USEEFFECT() - setIncomeList, setExpenseList
	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/categories`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data.length > 0){
				setIncomeList(data.map(income => {
					if (income.category_type === 'Income'){
						return (
							<option key={income._id}> {income.category_name} </option>
						)
					} else {
						return null
					}
				}))

				setExpenseList(data.map(expense => {
					if (expense.category_type === 'Expense'){
						return (
							<option key={expense._id}> {expense.category_name} </option>
						)
					} else {
						return null
					}
				}))
			} else {
				setIncomeList('')
				setExpenseList('')
			}
		})
	}, [categoryType, categoryName, amount, description, balance])

	function checkBalance(){
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/records`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data.length == 0){
				let currentBalance = 0

				if (categoryType === 'Income'){
					setBalance(parseInt(currentBalance) + parseInt(amount))
				} else if (categoryType === 'Expense'){
					setBalance(currentBalance - amount)
				}
			} else {
				let currentBalance = data[data.length - 1].record_balance

				if (categoryType === 'Income'){
					setBalance(parseInt(currentBalance) + parseInt(amount))
				} else if (categoryType === 'Expense'){
					setBalance(currentBalance - amount)
				}
			}
		})
	}

	// ADDNEWRECORD()
	function addNewRecord(e){
		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/records`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				category_type: categoryType,
		     category_name: categoryName,
		     amount: amount,
		     description: description,
		     record_balance: balance
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data === true){
				Swal.fire({
					icon: 'success',
					title: `"${description}" with an amount of "${amount}" added to "${categoryName}" successfully!`,
					text: 'Do you want to add more?',
					showDenyButton: true,
					confirmButtonText: `Yes`,
					denyButtonText: `Back to records`,
				})
				.then((result) => {
					if (result.isConfirmed) {
						setCategoryType('')
						setCategoryName('')
						setAmount('')
						setDescription('')

						Router.push('/records/addRecord')
					} else if (result.isDenied) {
						Router.push("/records")
					}
				})
			} else {
				Swal.fire({
					icon: 'error',
					text: 'Please try again.'
				})
			}
		})
	}

	return (
		<Fragment>
		<Head>
			<title> Add Record | Grapher+ </title>
		</Head>
		<Row>
			<Col lg={{span: 6, offset: 3}}>
				<Card id="cardform">
					<Card.Title>
						<h1> Add record </h1>
					</Card.Title>

					<Card.Body>
						<Form onSubmit={(e) => addNewRecord(e)}>
							<Form.Group controlId="categoryType">
								<Form.Label> Category Type: </Form.Label>
								<Form.Control
									as="select"
									value={categoryType}
									onChange={(e) => setCategoryType(e.target.value)}
									required
								>
									<option value="" disabled> Select category type </option>
									<option value="Income"> Income </option>
									<option value="Expense"> Expense </option>
								</Form.Control>
							</Form.Group>

							<Form.Group controlId="categoryName">
								<Form.Label> Category Name: </Form.Label>
								<Form.Control
									as="select"
									value={categoryName}
									onChange={(e) => setCategoryName(e.target.value)}
									required
								>
									<option value="" disabled> Select category name </option>
								{
									categoryType === "Income"
									?
									<Fragment>
									{incomeList}
									</Fragment>
									:
									<Fragment>
									{expenseList}
									</Fragment>
								}	
								</Form.Control>
							</Form.Group>		

							<Form.Group controlId="amount">
								<Form.Label> Record Amount: </Form.Label>
								<Form.Control
									type="number"
									value={amount}
									onChange={(e) => setAmount(e.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group controlId="description">
								<Form.Label> Record Description: </Form.Label>
								<Form.Control
									type="text"
									placeholder="Enter record description"
									maxLength="500"
									value={description}
									onChange={(e) => setDescription(e.target.value)}
									required
								/>
								<Form.Text className="text-muted"> Please limit your description to 500 characters. </Form.Text>
							</Form.Group>

							{
								isActive
								?
								<Button variant="outline-dark" type="submit" block> Add new record </Button>
								:
								<Button variant="dark" type="submit" block disabled> Add new record </Button>
							}

							<Button href="/records" variant="dark" block> Back to Records </Button>
						</Form>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		</Fragment>
	)
}