import {useState, useEffect, useContext, Fragment} from 'react'

import MonthlyIncome from '../components/MonthlyExpense'

import UserContext from '../UserContext'


export default function monthlyIncome(){
	// USER CONTEXT
	const {user, setUser} = useContext(UserContext)

	// STATE
	const [expense, setExpense] = useState([])
	
	// USEEFFECT()
	useEffect(()=>{
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/records`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setExpense(data)
		})
	}, [])

	return(
		<Fragment>
		<h2 className="my-4 text-center"> Monthly Expense in USD </h2>
		<MonthlyIncome expense={expense}/>
		</Fragment>
	)
}