import {useContext, useEffect} from 'react'

import Router from 'next/router'

import UserContext from '../UserContext'



export default function logout(){
	// USERCONTEXT
	const {unsetUser} = useContext(UserContext)

	// USEEFFECT()
	useEffect(() => {
		unsetUser()
		
		Router.push("/")
	}, [])

	return null
}