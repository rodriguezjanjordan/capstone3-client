import {useState, useEffect, Fragment} from 'react'
import Swal from 'sweetalert2'

import Head from 'next/head'
import Link from 'next/link'
import Router from 'next/router'

import {Card, Form, Button} from 'react-bootstrap'
import {Row, Col} from 'react-bootstrap'



export default function register(){
	// STATES
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')

	const [isActive, setIsActive] = useState(false)

	// USEEFFECT()
	useEffect(() => {
		if ((firstName !== '' && lastName !== '') &&
			(email !== '' && password1 !== '' && password2 !== '') &&
			(password1 == password2) && (password1.length > 7)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, email, password1, password2])

	// REGISTERUSER()
	function registerUser(e){
		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/email-exists`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data === false){
				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstname: firstName,
						lastname: lastName,
						email: email,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					if (data === true){
						Swal.fire({
							icon: 'success',
							title: 'REGISTRATION SUCCESS!',
							text: 'Thank you for registering.'
						})

						Router.push('/')
					} else {
						Swal.fire({
							icon: 'error',
							title: 'REGISTRATION FAILED!',
							text: 'Please try again.'
						})

						Router.push('/register')
					}
				})
			} else {
				Swal.fire({
					icon: 'warning',
					title: 'EMAIL ALREADY EXISTS!',
					text: 'What do you want to do next?',
					showDenyButton: true,
					confirmButtonText: `Login`,
					denyButtonText: `Register with new email`,
				}).then((result) => {
					if (result.isConfirmed) {
						Router.push('/login')
					} else if (result.isDenied) {
						Router.push('/register')
					}
				})
			}
		})
	}

	return (
		<Fragment>
		<Head>
		  <title> Register | Grapher+ </title>
		</Head>
		<Row>
			<Col lg={{span: 6, offset: 3}}>
				<Card id="cardform">
					<Card.Title>
						<h1> Register </h1>
					</Card.Title>

					<Card.Body>
						<Form onSubmit={(e) => registerUser(e)}>
							<Row>
								<Col sm={12} md={6}>
									<Form.Group controlId="firstName">
										<Form.Control
											type="text"
											placeholder="First name"
											value={firstName}
											onChange={(e) => setFirstName(e.target.value)}
										/>
									</Form.Group>
								</Col>

								<Col sm={12} md={6}>
									<Form.Group controlId="lastName">
										<Form.Control
											type="text"
											placeholder="Last name"
											value={lastName}
											onChange={(e) => setLastName(e.target.value)}
										/>
									</Form.Group>
								</Col>
							</Row>

							<Form.Group controlId="email">
								<Form.Control
									type="email"
									placeholder="Email address"
									value={email}
									onChange={(e) => setEmail(e.target.value)}
								/>
							</Form.Group>

							<Form.Group controlId="password1">
								<Form.Control
									type="password"
									placeholder="New password"
									value={password1}
									onChange={(e) => setPassword1(e.target.value)}
								/>
							</Form.Group>

							<Form.Group controlId="password2">
								<Form.Control
									type="password"
									placeholder="Re-type password"
									value={password2}
									onChange={(e) => setPassword2(e.target.value)}
								/>
							</Form.Group>

							{
								isActive
								?
								<Button variant="outline-dark" type="submit" block> Register </Button>
								:
								<Button variant="dark" type="submit" block disabled> Register </Button>
							}
						</Form>
					</Card.Body>
					<Card.Footer>
						<Form.Text className="text-muted"> Already have an account? <Link href="/">Login here.</Link> </Form.Text>
					</Card.Footer>
				</Card>
			</Col>
		</Row>
		</Fragment>
	)
}