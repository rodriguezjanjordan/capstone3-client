import {useState, useEffect, useContext, Fragment} from 'react'
import Swal from 'sweetalert2'

import Head from 'next/head'
import Link from 'next/link'
import Router from 'next/router'

import {GoogleLogin} from 'react-google-login'
import {Card, Form, Button} from 'react-bootstrap'
import {Row, Col} from 'react-bootstrap'

import UserContext from '../UserContext'



export default function login(){
  // USERCONTEXT
  const {user, setUser} = useContext(UserContext)

  // STATES
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const [isActive, setIsActive] = useState(false)

  // USEEFFECT()
  useEffect(() => {
    if ((email !== '' && password !== '') &&
      (password.length > 7)){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [email, password])

  // LOGINUSER()
  function loginUser(e){
    e.preventDefault()

    fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(res => res.json())
    .then(data => {
      if (data.access){
        localStorage.setItem('token', data.access)

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
          headers: {
            "Authorization": `Bearer ${data.access}`
          }
        })
        .then(res => res.json())
        .then(data => {
          setUser({id: data._id})

          Swal.fire({
            icon: 'success',
            title: 'LOGIN SUCCESS!'
          })

          Router.push('/categories')
        })
      } else {
        Swal.fire({
          icon: 'error',
          title: 'AUTHENTICATION FAILED!'
        })
      }
    })
  }

  // AUTHENTICATEGOOGLETOKEN()
  const authenticateGoogleToken = (response) => {
    const payload = {
      method: "post",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        tokenId: response.tokenId
      })
    }

    fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/google-login`, payload)
    .then(res => res.json())
    .then(data => {
      if (typeof data.access !== 'undefined'){
        localStorage.setItem('token', data.access)

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
          headers: {
            "Authorization": `Bearer ${data.access}`
          }
        })
        .then(res => res.json())
        .then(data => {
          setUser({
            id: data._id,
            email: data.email
          })

          Router.push("/categories")
        })
      } else {
        if (data.error === 'google-auth-error'){
          Swal.fire({
            icon: 'error',
            title: 'GOOGLE AUTHENTICATION ERROR!',
            text: 'Google Authentication Procedure has failed, try again or contact your web admin.'
          })
        } else if (data.error === 'login-type-error'){
          Swal.fire({
            icon: 'error',
            title: 'LOGIN TYPE ERROR!',
            text: 'You may have registered using a different login procedure. Try alternative login procedures.'
          })
        }
      }
    })
  }

  return (
    <Fragment>
    <Head>
      <title> Login | Grapher+ </title>
    </Head>
    <Row>
      <Col lg={{span: 6, offset: 3}}>
        <Card id="cardform">
          <Card.Title>
            <h1> Login </h1>
          </Card.Title>

          <Card.Body>
            <Form onSubmit={(e) => loginUser(e)}>
              <Form.Group controlId="email">
                <Form.Control
                  type="email"
                  placeholder="Email address"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
              </Form.Group>

              <Form.Group controlId="password">
                <Form.Control
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />
              </Form.Group>

              {
                isActive
                ?
                <Button variant="outline-dark" type="submit" block> Login </Button>
                :
                <Button variant="dark" type="submit" block disabled> Login </Button>
              }

              <GoogleLogin
                clientId="47765720487-fvc4sfs2ncgo1rnhrgcsgcui9ulfpvaa.apps.googleusercontent.com"
                buttonText="Login using Google"
                cookiePolicy={'single_host_origin'}
                onSuccess={authenticateGoogleToken}
                onFailure={authenticateGoogleToken}
                className="w-100 text-center d-flex justify-content-center googlelogin"
              />
            </Form>
          </Card.Body>
          <Card.Footer>
            <Form.Text className="text-muted"> Does not have an account yet? <Link href="/register">Register now!</Link> </Form.Text>
          </Card.Footer>
        </Card>
      </Col>
    </Row>
    </Fragment>
  )
}