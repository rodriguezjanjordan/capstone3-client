import {useState, useEffect, useContext, Fragment} from 'react'
import moment from 'moment'

import {Bar} from 'react-chartjs-2'

import UserContext from '../UserContext'



export default function MonthlyExpense({expense}){
	// USERCONTEXT
	const {user} = useContext(UserContext)

	// STATES
	const [months, setMonths] = useState([
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December"
	])
	const [monthlyExpense, setMonthlyExpense] = useState([])

	// USEEFFECT()
	useEffect(()=>{
		let arrJan = []
		let arrFeb = []
		let arrMar = []
		let arrApr = []
		let arrMay = []
		let arrJun = []
		let arrJul = []
		let arrAug = []
		let arrSep = []
		let arrOct = []
		let arrNov = []
		let arrDec = []

		setMonthlyExpense(expense.map(data => {
			if (moment(data.date).format('MMMM') === months[0] && data.category_type === "Expense"){
				arrJan.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[1] && data.category_type === "Expense"){
				arrFeb.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[2] && data.category_type === "Expense"){
				arrMar.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[3] && data.category_type === "Expense"){
				arrApr.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[4] && data.category_type === "Expense"){
				arrMay.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[5] && data.category_type === "Expense"){
				arrJun.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[6] && data.category_type === "Expense"){
				arrJul.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[7] && data.category_type === "Expense"){
				arrAug.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[8] && data.category_type === "Expense"){
				arrSep.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[9] && data.category_type === "Expense"){
				arrOct.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[10] && data.category_type === "Expense"){
				arrNov.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[11] && data.category_type === "Expense"){
				arrDec.push(data.amount)
			}
		}))

		let totalJan = arrJan.reduce((a,b) => a + b, 0)
		let totalFeb = arrFeb.reduce((a,b) => a + b, 0)
		let totalMar = arrMar.reduce((a,b) => a + b, 0)
		let totalApr = arrApr.reduce((a,b) => a + b, 0)
		let totalMay = arrMay.reduce((a,b) => a + b, 0)
		let totalJun = arrJun.reduce((a,b) => a + b, 0)
		let totalJul = arrJul.reduce((a,b) => a + b, 0)
		let totalAug = arrAug.reduce((a,b) => a + b, 0)
		let totalSep = arrSep.reduce((a,b) => a + b, 0)
		let totalOct = arrOct.reduce((a,b) => a + b, 0)
		let totalNov = arrNov.reduce((a,b) => a + b, 0)
		let totalDec = arrDec.reduce((a,b) => a + b, 0)
		let tempMonthlyExpense = []

		tempMonthlyExpense.splice(0, 0, totalJan)
		tempMonthlyExpense.splice(1, 0, totalFeb)
		tempMonthlyExpense.splice(2, 0, totalMar)
		tempMonthlyExpense.splice(3, 0, totalApr)
		tempMonthlyExpense.splice(4, 0, totalMay)
		tempMonthlyExpense.splice(5, 0, totalJun)
		tempMonthlyExpense.splice(6, 0, totalJul)
		tempMonthlyExpense.splice(7, 0, totalAug)
		tempMonthlyExpense.splice(8, 0, totalSep)
		tempMonthlyExpense.splice(9, 0, totalOct)
		tempMonthlyExpense.splice(10, 0, totalNov)
		tempMonthlyExpense.splice(11, 0, totalDec)

		setMonthlyExpense(tempMonthlyExpense)
	}, [expense])


	const data = {
		labels: months,
		datasets: [
			{
				label: 'Monthly Expense for the Year 2020',
				backgroundColor: '#8d99ae',
				borderColor: '#2b2d42',
				borderWidth: 2,
				hoverBackgroundColor: '#ef233c',
				hoverBorderColor: '#d90429',
				data: monthlyExpense
			}
		]
	}

	return (
		<Fragment>
		<Bar data={data} />
		</Fragment>
	)
}