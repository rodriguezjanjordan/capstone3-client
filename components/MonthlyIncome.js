import {useState, useEffect, useContext, Fragment} from 'react'
import moment from 'moment'

import {Bar} from 'react-chartjs-2'

import UserContext from '../UserContext'



export default function MonthlyIncome({income}){
	// USERCONTEXT
	const {user} = useContext(UserContext)

	// STATES
	const [months, setMonths] = useState([
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December"
	])
	const [monthlyIncome, setMonthlyIncome] = useState([])

	// USEEFFECT()
	useEffect(()=>{
		let arrJan = []
		let arrFeb = []
		let arrMar = []
		let arrApr = []
		let arrMay = []
		let arrJun = []
		let arrJul = []
		let arrAug = []
		let arrSep = []
		let arrOct = []
		let arrNov = []
		let arrDec = []

		setMonthlyIncome(income.map(data => {
			if (moment(data.date).format('MMMM') === months[0] && data.category_type === "Income"){
				arrJan.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[1] && data.category_type === "Income"){
				arrFeb.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[2] && data.category_type === "Income"){
				arrMar.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[3] && data.category_type === "Income"){
				arrApr.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[4] && data.category_type === "Income"){
				arrMay.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[5] && data.category_type === "Income"){
				arrJun.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[6] && data.category_type === "Income"){
				arrJul.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[7] && data.category_type === "Income"){
				arrAug.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[8] && data.category_type === "Income"){
				arrSep.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[9] && data.category_type === "Income"){
				arrOct.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[10] && data.category_type === "Income"){
				arrNov.push(data.amount)
			} else if (moment(data.date).format('MMMM') === months[11] && data.category_type === "Income"){
				arrDec.push(data.amount)
			}
		}))

		let totalJan = arrJan.reduce((a,b) => a + b, 0)
		let totalFeb = arrFeb.reduce((a,b) => a + b, 0)
		let totalMar = arrMar.reduce((a,b) => a + b, 0)
		let totalApr = arrApr.reduce((a,b) => a + b, 0)
		let totalMay = arrMay.reduce((a,b) => a + b, 0)
		let totalJun = arrJun.reduce((a,b) => a + b, 0)
		let totalJul = arrJul.reduce((a,b) => a + b, 0)
		let totalAug = arrAug.reduce((a,b) => a + b, 0)
		let totalSep = arrSep.reduce((a,b) => a + b, 0)
		let totalOct = arrOct.reduce((a,b) => a + b, 0)
		let totalNov = arrNov.reduce((a,b) => a + b, 0)
		let totalDec = arrDec.reduce((a,b) => a + b, 0)
		let tempMonthlyIncome = []

		tempMonthlyIncome.splice(0, 0, totalJan)
		tempMonthlyIncome.splice(1, 0, totalFeb)
		tempMonthlyIncome.splice(2, 0, totalMar)
		tempMonthlyIncome.splice(3, 0, totalApr)
		tempMonthlyIncome.splice(4, 0, totalMay)
		tempMonthlyIncome.splice(5, 0, totalJun)
		tempMonthlyIncome.splice(6, 0, totalJul)
		tempMonthlyIncome.splice(7, 0, totalAug)
		tempMonthlyIncome.splice(8, 0, totalSep)
		tempMonthlyIncome.splice(9, 0, totalOct)
		tempMonthlyIncome.splice(10, 0, totalNov)
		tempMonthlyIncome.splice(11, 0, totalDec)

		setMonthlyIncome(tempMonthlyIncome)
	}, [income])


	const data = {
		labels: months,
		datasets: [
			{
				label: 'Monthly Income for the Year 2020',
				backgroundColor: '#8d99ae',
				borderColor: '#2b2d42',
				borderWidth: 2,
				hoverBackgroundColor: '#ef233c',
				hoverBorderColor: '#d90429',
				data: monthlyIncome
			}
		]
	}

	return (
		<Fragment>
		<Bar data={data} />
		</Fragment>
	)
}