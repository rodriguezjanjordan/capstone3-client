import {useContext, Fragment} from 'react'
import Link from 'next/link'

import {Navbar, Nav} from 'react-bootstrap'

import UserContext from '../UserContext'

export default function NavBar(){
	// USERCONTEXT
	const {user} = useContext(UserContext)

	return (
		<Navbar bg="dark" variant="dark" expand="lg">
			<Link href="/"><a className="navbar-brand"> Grapher+ </a></Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav" >
				<Nav className="ml-auto">
					{
						user.id === null
						?
						<Fragment>
						</Fragment>
						:
						<Fragment>
						<Link href="/categories"><a className="nav-link"> Categories </a></Link>
						<Link href="/records"><a className="nav-link"> Records </a></Link>
						<Link href="/monthlyIncome"><a className="nav-link"> Monthly Income </a></Link>
						<Link href="/monthlyExpense"><a className="nav-link"> Monthly Expense </a></Link>
						<Link href="/logout"><a className="nav-link"> Logout </a></Link>
						</Fragment>
					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}